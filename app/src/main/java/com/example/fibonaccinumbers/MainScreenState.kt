package com.example.fibonaccinumbers

data class MainScreenState(
    val result: String = "",
    val isCalculating: Boolean = false,
    val btnText: String = "Start",
    val isEditTextEnabled: Boolean = true
)