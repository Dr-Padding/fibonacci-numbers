package com.example.fibonaccinumbers

import android.app.AlarmManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent

class AlarmSchedularImpl(
    private val context: Context,
) : AlarmSchedular {

    private val alarmManager = context.getSystemService(AlarmManager::class.java)

    override fun schedule() {
        val intent = Intent(context, AlarmReceiver::class.java)
        alarmManager.setExactAndAllowWhileIdle(
            AlarmManager.RTC_WAKEUP,
            2000,
            PendingIntent.getBroadcast(
                context,
                FIBONACCI_CALCULATION_ALARM_ID,
                intent,
                PendingIntent.FLAG_UPDATE_CURRENT or PendingIntent.FLAG_IMMUTABLE
            )
        )
    }

    companion object {
        const val FIBONACCI_CALCULATION_ALARM_ID = 100
    }
}