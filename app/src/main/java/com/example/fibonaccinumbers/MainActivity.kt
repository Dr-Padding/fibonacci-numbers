package com.example.fibonaccinumbers

import android.os.Bundle
import android.widget.Toast
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.lifecycleScope
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.fibonaccinumbers.databinding.ActivityMainBinding
import kotlinx.coroutines.launch

class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::bind)
    private val viewModel: MainViewModel by viewModels()
    private var isCalculating = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        initViews()

        lifecycleScope.launch {
            viewModel.state.collect { state ->
                binding.apply {
                    tvResult.text = state.result
                    etInputNumber.isEnabled = state.isEditTextEnabled
                    btnStart.text = state.btnText
                    isCalculating = state.isCalculating
                }
            }
        }

        lifecycleScope.launch {
            viewModel.event.collect { event ->
                Toast.makeText(this@MainActivity, event, Toast.LENGTH_SHORT).show()
            }
        }
    }

    private fun initViews() {

        val schedular = AlarmSchedularImpl(this@MainActivity)

        binding.apply {
            btnStart.setOnClickListener {
                if (!isCalculating) {
                    try {
                        val inputNumber = etInputNumber.text.toString().toInt()
                        viewModel.startFibonacciCalculation(inputNumber)
                    } catch (e: java.lang.NumberFormatException) {
                        Toast.makeText(
                            this@MainActivity,
                            "Please enter a valid number",
                            Toast.LENGTH_SHORT
                        ).show()
                    }
                } else {
                    viewModel.cancelCalculation()
                }
            }

            btnDeferredStart.setOnClickListener {
                schedular.schedule()
            }
        }
    }

}


