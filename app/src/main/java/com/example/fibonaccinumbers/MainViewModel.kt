package com.example.fibonaccinumbers

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.*
import kotlinx.coroutines.flow.*

class MainViewModel : ViewModel() {

    private val _state = MutableStateFlow(MainScreenState())
    val state: StateFlow<MainScreenState> = _state

    private val _event = MutableSharedFlow<String>()
    val event: SharedFlow<String> = _event

    private var job: Job? = null

    fun startFibonacciCalculation(limit: Int) {
        job = viewModelScope.launch(Dispatchers.Default) {
            try {
                _state.value = state.value.copy(
                    isEditTextEnabled = false,
                    btnText = "Cancel",
                    isCalculating = true
                )
                var prev = 0
                var curr = 1
                _state.value = state.value.copy(
                    result = "Current is $prev",
                    btnText = "Cancel",
                    isCalculating = true,
                    isEditTextEnabled = false
                )
                delay(1000L)
                _state.value = state.value.copy(
                    result = "Current is $curr",
                    btnText = "Cancel",
                    isCalculating = true,
                    isEditTextEnabled = false
                )
                delay(1000L)
                for (i in 2..limit) {
                    val next = prev + curr
                    prev = curr
                    curr = next
                    if (i == limit) {
                        _state.value = state.value.copy(
                            result = "Result is $next",
                            isEditTextEnabled = true,
                            btnText = "Start",
                            isCalculating = false
                        )
                    } else {
                        _state.value = state.value.copy(
                            result = "Current is $next",
                            btnText = "Cancel",
                            isCalculating = true,
                            isEditTextEnabled = false
                        )
                    }
                    delay(1000L)
                }
            } catch (e: Exception) {
                _event.emit("Calculation error")
            } finally {
                job = null
            }
        }
    }

    fun cancelCalculation() {
        job?.cancel()
        _state.value = MainScreenState()
    }
}



